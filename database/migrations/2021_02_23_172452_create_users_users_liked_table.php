<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersUsersLikedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_users_liked', function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('user_liked_id');
            $table->timestamps();

            $table->foreign('user_id')->references("id")->on("users");
            $table->foreign('user_liked_id')->references("id")->on("users");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_users_liked');
    }
}
