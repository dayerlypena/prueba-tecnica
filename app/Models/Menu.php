<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    protected $table = "menus";
    protected $fillable = ['label', 'url', 'icon','parent_id'];


    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }

}
