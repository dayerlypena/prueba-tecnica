<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $fillable = ['name'];

    /*
     * Usuario a quien dio like
     */
    public function likesToUsers()
    {
        return $this->belongsToMany(self::class, 'users_users_liked', 'user_id', 'user_liked_id');
    }

    /*
     * usuario quien recibio el like
     */

    public function likesFromUsers()
    {
        return $this->belongsToMany(self::class, 'users_users_liked', 'user_liked_id', 'user_id');
    }

    public function matches()
    {
        return $this->likesFromUsers()->whereIn('user_id', $this->likesToUsers()->get());
    }
}
