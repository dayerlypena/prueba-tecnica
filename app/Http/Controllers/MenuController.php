<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {

        $menu = Menu::with(implode('.', array_fill(0, 100, 'parent')))
            ->where('id', '!=', 0)->get();

        return json_decode($menu);

    }


}
